package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)
	sum := int64(0)
	for fileScanner.Scan() {
		text := fileScanner.Text()
		winningNumbers := make(map[int64]bool, 0)
		myNumbers := make(map[int64]bool, 0)
		verticalSplit := strings.Split(text, "|")
		winningNumbersSplit := strings.Split(strings.Trim(strings.Split(verticalSplit[0], ":")[1], " "), " ")
		myNumbersSplit := strings.Split(strings.Trim(verticalSplit[1], " "), " ")
		for _, strVal := range winningNumbersSplit {
			if len(strVal) == 0 {
				continue
			}
			winningNumbers[getIntVal(strVal)] = true
		}
		for _, strVal := range myNumbersSplit {
			if len(strVal) == 0 {
				continue
			}
			myNumbers[getIntVal(strVal)] = true
		}
		points := int64(0)
		for intVal := range winningNumbers {
			_, found := myNumbers[intVal]
			if found {
				if points == 0 {
					points++
				} else {
					points *= 2
				}
			}
		}
		sum += points
	}
	println(sum)
}

func getIntVal(str string) int64 {
	intStrVal := strings.Trim(str, " ")
	number, err := strconv.ParseInt(intStrVal, 10, 64)
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}
	return number
}
