package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)

	lines := make([]int64, 0)
	for fileScanner.Scan() {
		text := fileScanner.Text()
		lines = append(lines, GetWins(text))
	}
	total := int64(0)
	for i := 0; i < len(lines); i++ {
		wins := lines[i]

		TraverseTree(i+1, wins, lines, &total)
		total++
	}
	println(total)
}

func TraverseTree(index int, wins int64, lines []int64, total *int64) {
	for j := 0; j < int(wins); j++ {
		nextWins := lines[index+j]
		TraverseTree(index+j+1, nextWins, lines, total)
		*total++
	}
}

func GetWins(text string) int64 {
	winningNumbers := make(map[int64]bool, 0)
	myNumbers := make(map[int64]bool, 0)
	verticalSplit := strings.Split(text, "|")
	winningNumbersSplit := strings.Split(strings.Trim(strings.Split(verticalSplit[0], ":")[1], " "), " ")
	myNumbersSplit := strings.Split(strings.Trim(verticalSplit[1], " "), " ")
	for _, strVal := range winningNumbersSplit {
		if len(strVal) == 0 {
			continue
		}
		winningNumbers[getIntVal(strVal)] = true
	}
	for _, strVal := range myNumbersSplit {
		if len(strVal) == 0 {
			continue
		}
		myNumbers[getIntVal(strVal)] = true
	}
	wins := int64(0)
	for intVal := range winningNumbers {
		_, found := myNumbers[intVal]
		if found {
			wins++
		}
	}
	return wins
}

func getIntVal(str string) int64 {
	intStrVal := strings.Trim(str, " ")
	number, err := strconv.ParseInt(intStrVal, 10, 64)
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}
	return number
}
