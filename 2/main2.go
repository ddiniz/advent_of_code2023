package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)
	idSum := int64(0)
	lineNum := int64(1)
	for fileScanner.Scan() {
		text := fileScanner.Text()
		gameHands := strings.Split(strings.Split(text, ":")[1], ";")

		maxRed := int64(0)
		maxBlue := int64(0)
		maxGreen := int64(0)
		for _, hand := range gameHands {
			cubeTypes := strings.Split(hand, ",")
			for _, cType := range cubeTypes {
				val := getIntVal(cType)
				if strings.Contains(cType, "red") {
					if maxRed < val {
						maxRed = val
					}
				} else if strings.Contains(cType, "green") {
					if maxGreen < val {
						maxGreen = val
					}
				} else if strings.Contains(cType, "blue") {
					if maxBlue < val {
						maxBlue = val
					}
				} else {
					println("error:", hand)
					os.Exit(1)
				}
			}
		}
		idSum += maxRed * maxBlue * maxGreen
		println(maxRed * maxBlue * maxGreen)
		lineNum++
	}
	println(idSum)
	return
}

func getIntVal(str string) int64 {
	intStrVal := strings.Split(strings.Trim(str, " "), " ")[0]
	number, err := strconv.ParseInt(intStrVal, 10, 64)
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}
	return number
}
