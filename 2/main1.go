package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)
	idSum := int64(0)
	lineNum := int64(1)
	for fileScanner.Scan() {
		text := fileScanner.Text()
		gameHands := strings.Split(strings.Split(text, ":")[1], ";")

		invalid := false
		for _, hand := range gameHands {
			red := int64(12)
			green := int64(13)
			blue := int64(14)
			cubeTypes := strings.Split(hand, ",")
			for _, cType := range cubeTypes {
				if strings.Contains(cType, "red") {
					red -= getIntVal(cType)
				} else if strings.Contains(cType, "green") {
					green -= getIntVal(cType)
				} else if strings.Contains(cType, "blue") {
					blue -= getIntVal(cType)
				} else {
					println("error:", hand)
					os.Exit(1)
				}
			}
			if red < 0 || green < 0 || blue < 0 {
				invalid = true
			}
		}
		if !invalid {
			idSum += lineNum
		}
		lineNum++
	}
	println(idSum)
	return
}

func getIntVal(str string) int64 {
	intStrVal := strings.Split(strings.Trim(str, " "), " ")[0]
	number, err := strconv.ParseInt(intStrVal, 10, 64)
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}
	return number
}
