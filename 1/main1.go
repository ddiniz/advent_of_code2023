package main

import (
	"bufio"
	"os"
	"strconv"
	"unicode"
)

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)
	sum := int64(0)
	for fileScanner.Scan() {
		text := fileScanner.Text()
		var first rune
		for _, r := range text {
			if unicode.IsNumber(r) {
				first = r
				break
			}
		}
		var last rune

		for i := len(text) - 1; i >= 0; i-- {
			if unicode.IsNumber(rune(text[i])) {
				last = rune(text[i])
				break
			}
		}
		full := string([]rune{first, last})
		if len(full) > 0 {
			number, err := strconv.ParseInt(full, 10, 64)
			if err != nil {
				println(err.Error())
				os.Exit(1)
			}
			sum += number
		}
	}
	println(sum)
	return
}
