package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

const (
	ZERO  = "zero"
	ONE   = "one"
	TWO   = "two"
	THREE = "three"
	FOUR  = "four"
	FIVE  = "five"
	SIX   = "six"
	SEVEN = "seven"
	EIGHT = "eight"
	NINE  = "nine"
)

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)
	sum := int64(0)
	for fileScanner.Scan() {
		text := fileScanner.Text()
		var first rune
		for i, s := range text {
			if unicode.IsLetter(s) {
				foundString := ChopLeftWhile(text, &i, unicode.IsLetter)
				numberRune, err := wordToNumberRune(foundString, true)
				if err != nil {
					continue
				}
				first = numberRune
				break
			}
			if unicode.IsNumber(s) {
				first = s
				break
			}
		}

		var last rune
		for i := len(text) - 1; i >= 0; i-- {
			s := rune(text[i])
			if unicode.IsLetter(s) {
				foundString := ChopRightWhile(text, &i, unicode.IsLetter)
				numberRune, err := wordToNumberRune(foundString, false)
				if err != nil {
					continue
				}
				last = numberRune
				break
			}
			if unicode.IsNumber(s) {
				last = s
				break
			}
		}
		full := string([]rune{first, last})
		if len(full) > 0 {
			number, err := strconv.ParseInt(full, 10, 64)
			if err != nil {
				println(err.Error())
				os.Exit(1)
			}
			sum += number
		}
	}
	println(sum)
	return
}

func wordToNumberRune(str string, leftToRight bool) (rune, error) {
	numbers := []string{ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE}
	runes := []rune{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}
	if leftToRight {
		smallestIndex := 99999
		smallestPos := -1
		for i, num := range numbers {
			pos := strings.Index(str, num)
			if pos != -1 && pos < smallestIndex {
				smallestIndex = pos
				smallestPos = i
			}
		}
		if smallestPos == -1 {
			return '?', fmt.Errorf("couldn't find any written numbers")
		}
		return runes[smallestPos], nil
	} else {
		biggestIndex := -1
		biggestPos := -1
		for i, num := range numbers {
			pos := strings.LastIndex(str, num)
			if pos != -1 && pos > biggestIndex {
				biggestIndex = pos
				biggestPos = i
			}
		}
		if biggestPos == -1 {
			return '?', fmt.Errorf("couldn't find any written numbers")
		}
		return runes[biggestPos], nil
	}
}

func ChopLeftWhile(text string, index *int, test func(x rune) bool) string {
	i := *index
	for i < len(text) && test(rune(text[i])) {
		i += 1
	}
	*index = i
	return text[:*index]
}

func ChopRightWhile(text string, index *int, test func(x rune) bool) string {
	i := *index
	for i > 0 && test(rune(text[i])) {
		i -= 1
	}
	*index = i + 1
	return text[*index:]
}
