package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type Vector2 struct {
	X int
	Y int
}

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	symbolPositions := make([]*Vector2, 0)
	inputMatrix := make([][]rune, 0)

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)
	lineNum := 0
	for fileScanner.Scan() {
		text := fileScanner.Text()
		inputMatrix = append(inputMatrix, make([]rune, len(text)))
		for i, r := range text {
			if IsSymbol(r) {
				symbolPositions = append(symbolPositions, &Vector2{X: i, Y: lineNum})
			}
			inputMatrix[lineNum][i] = r
		}
		lineNum++
	}

	numberPositions := make([]*Vector2, 0)
	for _, vec := range symbolPositions {
		numberPositions = append(numberPositions, CheckAround(vec, inputMatrix)...)
	}

	numbers := make([]string, 0)
	aux := make([]*Vector2, 0)
	for _, pos := range numberPositions {
		start := 0
		end := len(inputMatrix[pos.Y])
		for i := pos.X; i >= 0; i-- {
			r := inputMatrix[pos.Y][i]
			if !unicode.IsDigit(r) {
				start = i + 1
				break
			}
		}
		for i := pos.X; i < len(inputMatrix[pos.Y]); i++ {
			r := inputMatrix[pos.Y][i]
			if !unicode.IsDigit(r) {
				end = i
				break
			}
		}
		numberText := string(inputMatrix[pos.Y][start:end])
		for k := 0; k < end-start; k++ {
			aux = append(aux, &Vector2{X: start + k, Y: pos.Y})
		}

		numbers = append(numbers, numberText)
	}

	//Prints out the input with every number that has been accounted for as blank spaces.
	for j, line := range inputMatrix {
		for i, r := range line {
			found := false
			for _, a := range aux {
				if a.X == i && a.Y == j {
					print(" ")
					found = true
					break
				}
			}
			if !found {
				print(string(r))
			}
		}
		print("\n")
	}

	sum := int64(0)
	for _, strVal := range numbers {
		sum += getIntVal(strVal)
	}
	println(sum)
	return
}

func IsSymbol(r rune) bool {
	return r != '.' && !unicode.IsNumber(r)
}

func getIntVal(str string) int64 {
	intStrVal := strings.Split(strings.Trim(str, " "), " ")[0]
	number, err := strconv.ParseInt(intStrVal, 10, 64)
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}
	return number
}

var DIRECTIONS = []*Vector2{ //try to figure out this shit, buddy. Good luck
	{-1, -1}, //a - 0
	{+0, -1}, //h - 1
	{+1, -1}, //g - 2
	{-1, +0}, //h - 3
	{-1, +1}, //g - 4
	{+0, +1}, //9 - 5
	{+1, +1}, //9 - 6
	{+1, +0}, //d - 7
}

func CheckAround(vec *Vector2, inputMatrix [][]rune) []*Vector2 {
	digitPositions := make([]*Vector2, 0)
	lastWasDigit := false
	for i, dir := range DIRECTIONS {
		r := inputMatrix[vec.Y+dir.Y][vec.X+dir.X]
		if InTopRowDirection(i) || InBottomRowDirection(i) {
			if unicode.IsDigit(r) {
				if !lastWasDigit {
					digitPositions = append(digitPositions, &Vector2{X: vec.X + dir.X, Y: vec.Y + dir.Y})
					lastWasDigit = true
				}
			} else {
				lastWasDigit = false
			}
		} else {
			if unicode.IsDigit(r) {
				digitPositions = append(digitPositions, &Vector2{X: vec.X + dir.X, Y: vec.Y + dir.Y})
			}
			lastWasDigit = false
		}

	}
	return digitPositions
}

func InTopRowDirection(i int) bool {
	return i == 0 || i == 1 || i == 2
}
func InBottomRowDirection(i int) bool {
	return i == 4 || i == 5 || i == 6
}
